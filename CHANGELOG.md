# Changelog
All notable changes to this project will be documented in this file. Loosely based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/). [Semantic Versioning](https://semver.org/spec/v2.0.0.html) is being used.

## 3.0.2 - 2023-03-16
### Fixed (Anichart)
- Active tabs and options color
- Ticks follow the user-set accent color
- Code is no longer messy
- Login button easier to read
- Night theme selector is now black
- Colors match Black Anilist

### Added (Anichart)
- Option to change font color if the text becomes too hard to read over the accent color
- Improved settings mouse hover animation
- Added license

## 3.0.1 - 2023-03-15
### Fixed
- Comment and like buttons were blue

## 3.0.0 - 2023-03-15
### Fixed
- Version number background is now black
- Quick search results hover background fixed
- Mod badges text color

### Added
- License

## 3.0.0-a.2 - 2023-03-15
### Fixed
- Hearts are no longer white
- Activity history has better colors
- Code has been cleaned and is overall improved
- Titles in lists now have the correct color and light up when the line itself is hovered
- Dropdown colors and animations

## 2.0.8 - 2022-02-02
### Fixed
- Mod badges
- Search tag color & hover text color
- Search year & season text color
- Search dropdown colors (text, placeholders, titles, ...)
- Search 'clear all' button
- 'Sort by' dropdown
- Quick search icons

### Added
- Collapse bar themed as well

## 2.0.5 - 2021-09-06
### Fixed
- Blue profiles activity history

## 2.0.3 - 2021-07-26
### Fixed
- Character & staff page header

## 2.0.0 - 2021-07-20
### Added
- Moved to master branch!

## 2.0.0-b.10 - 2021-07-20
### Added
- Separate browse section

### Fixed
- Settings, forum nav hover color
- Create thread text color
- User dropdown texts
- Navbar texts and search

## 2.0.0-b.9 - 2021-07-17
### Added
- Updated Anichart to v2
- Hovering a notification highlights it

### Fixed (Anilist)
- Mark all as read button
- Notifications overhaul
- Infobox button text color

### Fixed (Anichart)
- Navbar text color
- Accent color

## 2.0.0-b.8 - 2021-06-30
### Fixed
- Profile banner shadow
- Hamburger shadow

## 2.0.0-b.7 - 2021-06-11
### Added
- This changelog.

### Changed
- Updated beta README.

### Fixed
- "Mark all as read" text color.
- Blue profiles now stay blue.
- Deletion buttons.
- Titles in lists when focusing on a category.
- Edit buttom follow the set text color.
- Various details.